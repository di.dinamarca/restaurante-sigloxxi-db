from dataclasses import dataclass
from django.shortcuts import render, get_object_or_404
from .forms import BoletaForm, BoletaInsumosForm
from calendar import c
from csv import list_dialects
from email.policy import default
from functools import partial
import imp
import json
from pickle import LIST
import re
from urllib import request, response
from django.shortcuts import render, redirect
from django.contrib import auth
from .serializer import *
from .models import * 
from rest_framework.response import Response
from Restorant import serializer
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer
from datetime import date, datetime
from django.db.models import F



# Create your views here.

# @api_view(["POST"])
# def ingresarProducto(request):
#     if request.method == 'POST':
#         request.data["prod_fecha_ingreso"] = datetime.now()
#         producto_serializer = ProductosSerializers(data=request.data)
#         if producto_serializer.is_valid():
#             producto_serializer.save()
#             print ('hace todo bien')
#             return Response("agregado correctamente")
#         return Response("no se agrego")

# @api_view(["GET"])
# def listaProducto(request):
#     if request.method == 'GET':
#         Prod = Productos.objects.values()
#         return Response(Prod)

""" @api_view(["GET"])
def bolInsumo(request):
    if request.method == 'GET':
        bolInsumo = Boletainsumos.objects.values()
        return Response(bolInsumo) """

#Bodega

def loginBodega(request):
    return render(request, 'bodega/loginBodega.html')

def menuBodega(request):
    return render(request, 'bodega/menuBodega.html')

def inventarioBodega(request):
    return render(request, 'bodega/inventarioBodega.html')

def ingresarBodega(request):
    return render(request, 'bodega/ingresarProductoBodega.html')

def eliminarBodega(request):
    return render(request, 'bodega/eliminarProductoBodega.html')

def modificarBodega(request):
    return render(request, 'bodega/modificarProductoBodega.html')

def solicitarBodega(request):
    return render(request, 'bodega/solicitarInsumosBodega.html')

def proveedoresBodega(request):
    return render(request, 'bodega/proveedoresBodega.html')

#Finanzas

def index(request):
    return render (request, 'finanzas/index.html')

def emitirBoleta(request):
    data = {
        'form': BoletaForm()
    }

    if request.method == 'POST':
        formulario = BoletaForm(data=request.POST, files= request.FILES)
        if formulario.is_valid():
            formulario.save()
            data["mensaje"] = "guardado correctamente"
        else:
            data["form"] = formulario

    return render (request, 'finanzas/emitirBoleta.html', data)

def emitirBoletaInsumos(request):

    data = {
        'form': BoletaInsumosForm()
    }

    if request.method == 'POST':
        formulario = BoletaInsumosForm(data=request.POST, files= request.FILES)
        if formulario.is_valid():
            formulario.save()
            data["mensaje"] = "guardado correctamente"
        else:
            data["form"] = formulario


    return render(request, 'finanzas/emitirBoletaInsumos.html', data)

def verBoletas(request):
    boletaVenta = Boleta.objects.all()
    data = {
        'boletaVenta' : boletaVenta
    }
    return render (request, 'finanzas/verBoletas.html', data)

def verBoletasInsumos(request):
    boletaInsumos = BoletaInsumos.objects.all()
    data = {
        'boletaInsumos' : boletaInsumos
    }
    return render(request, 'finanzas/verBoletasInsumos.html', data)

def eliminarBoletaVenta(request, id):

    boletaVenta = get_object_or_404(Boleta, bol_id =id)
    boletaVenta.delete()
    return redirect(to="verBoletas")


def eliminarBoletaInsumo(request, id):

    boletaInsumos = get_object_or_404(BoletaInsumos, bol_ins_id =id)
    boletaInsumos.delete()
    
    return redirect(to="verBoletasInsumos")

def modificarBoletaI(request, id):

    boletaInsumos = get_object_or_404(BoletaInsumos, bol_ins_id =id)

    data = {
        'form' : BoletaInsumosForm(instance= boletaInsumos)
    }

    if request.method == 'POST':
        formulario = BoletaInsumosForm(data=request.POST, instance=boletaInsumos)
        if formulario.is_valid():
            formulario.save()
            return redirect(to="verBoletasInsumos")
        data["form"] = formulario

    return render (request,'finanzas/modificarBoletaV.html', data)


def modificarBoletaV(request, id):

    boletaVenta = get_object_or_404(Boleta, bol_id =id)

    data = {
        'form' : BoletaForm(instance= boletaVenta)
    }

    if request.method == 'POST':
        formulario = BoletaForm(data=request.POST, instance=boletaVenta)
        if formulario.is_valid():
            formulario.save()
            return redirect(to="verBoletas")
        data["form"] = formulario

    return render (request,'finanzas/modificarBoletaV.html', data)

def registroHistoricoVentas(request):
    return render(request, 'finanzas/registroHistoricoVentas.html')

def registroHistoricoCompras(request):
    return render(request, 'finanzas/registroHistoricoCompras.html')

def detallesRegistro(request):
    return render(request, 'finanzas/detallesRegistro.html')


def informes(request):
    return render (request, 'finanzas/informes.html')

def ganancias(request):
    return render (request, 'finanzas/ganancias.html')
# Cocina
def ingresarReceta(request):
    return render(request, 'cocina/ingresarReceta.html')

def receta(request):
    return render(request, 'cocina/receta.html')

def inicio(request):
    return render(request, 'cocina/inicio.html')

def Pedido(request):
    return render(request, 'cocina/Pedido.html')

def ModificarReceta(request):
    return render(request, 'cocina/ModificarReceta.html')

def Eliminar(request):
    return render(request, 'cocina/Eliminar.html')


#Carrito
def indexC(request):
    return render(request, 'carrito/index.html')