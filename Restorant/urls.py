
from django.urls import path
from .views import *
from Restorant import views


urlpatterns = [
    # path('inventario/', views.inventario),
    # path('login/', views.login),
    # path('datosGenerales/', views.datosGenerales),
    # path('menuAdm/',  views.menuAdm),
    # path('ingresarProd/', views.ingresarProd),

    # axios
    #path('listaProducto/', views.listaProducto),
    #path('ingresarProducto/', views.ingresarProducto),

    #Bodega
    path('loginBodega/', views.loginBodega),
    path('menuBodega/', views.menuBodega, name='menuBodega'),
    path('inventarioBodega/', views.inventarioBodega, name='inventarioBodega'),
    path('ingresarProductoBodega/', views.ingresarBodega, name='ingresarBodega'),
    path('eliminarProductoBodega/', views.eliminarBodega, name='eliminarBodega'),
    path('modificarProductoBodega/', views.modificarBodega, name='modificarBodega'),
    path('solicitarInsumosBodega/', views.solicitarBodega, name='solicitarBodega'),
    path('proveedoresBodega/', views.proveedoresBodega, name='proveedoresBodega'),

    #Finanzas
    path('index/',views.index, name='index'),
    path('emitirBoleta/', views.emitirBoleta, name='emitirBoleta'),
    path('emitirBoletaInsumos/', views.emitirBoletaInsumos, name='emitirBoletaInsumos'),
    path('verBoletas/', views.verBoletas, name='verBoletas'),
    path('verBoletasInsumos/', views.verBoletasInsumos, name='verBoletasInsumos'),
    path('eliminarBoletaVenta/<id>/', views.eliminarBoletaVenta, name='eliminarBoletaVenta'),
    path('eliminarBoletaInsumo/<id>/', views.eliminarBoletaInsumo, name='eliminarBoletaInsumo'),
    path('registroHistoricoVentas/', views.registroHistoricoVentas),
    path('registroHistoricoCompras/', views.registroHistoricoCompras, name='registroHistoricoCompras'),
    path('detallesRegistro/', views.detallesRegistro, name='detallesRegistro'),
    path('informes/', views.informes, name='informes'),
    path('ganancias/', views.ganancias, name='ganancias'),
    path('modificarBoletaV/<id>/', views.modificarBoletaV, name='modificarBoletaV'),
    path('modificarBoletaI/<id>/', views.modificarBoletaI, name='modificarBoletaI'),

    #Cocina
    path('ingresarReceta/',views.ingresarReceta, name='ingresarReceta'),
    path('receta/',views.receta, name='receta'),
    path('inicio/',views.inicio, name='inicio'),
    path('Eliminar/',views.Eliminar, name='Eliminar'),
    path('ModificarReceta/',views.ModificarReceta, name='ModificarReceta'),
    path('Pedido/',views.Pedido, name='Pedido'),

    #Carrito
    path('indexC/',views.indexC, name='indexC')
]
