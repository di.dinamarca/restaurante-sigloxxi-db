from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Receta)
admin.site.register(BoletaInsumos)
admin.site.register(Boleta)
admin.site.register(Cargo)
admin.site.register(Trabajadores)
admin.site.register(Reserva)
admin.site.register(Cliente)
admin.site.register(Proveedor)
admin.site.register(Pedido)
admin.site.register(ProdInsumos)
admin.site.register(CategoriaProducto)

