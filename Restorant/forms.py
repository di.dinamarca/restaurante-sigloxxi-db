from dataclasses import field, fields
from django import forms
from .models import *

class BoletaForm(forms.ModelForm):
    class Meta:
        model = Boleta
        fields = '__all__'


class BoletaInsumosForm(forms.ModelForm):
    class Meta:
        model = BoletaInsumos
        fields = '__all__'