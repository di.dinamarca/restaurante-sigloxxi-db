from enum import _auto_null
from sre_parse import Verbose
from tokenize import blank_re
from django.db import models
from datetime import datetime, date

# Create your models here.


#generadas desde bd



class Boleta(models.Model):
    bol_id = models.AutoField(primary_key=True, auto_created=True, verbose_name= 'ID Boleta')
    bol_fecha = models.DateTimeField(auto_now_add=True, verbose_name='Fecha')
    bol_cant = models.CharField(max_length=3, verbose_name='Cantidad')
    bol_subtotal = models.CharField(max_length=7,verbose_name='Subtotal')
    bol_iva = models.CharField(max_length=7, verbose_name='IVA')
    bol_totalboleta = models.CharField(max_length=7,verbose_name='Total Boleta')
    bol_productoconsumido = models.CharField(max_length=100,verbose_name='Producto Consumido')
    pedido_ped = models.ForeignKey('Pedido', models.DO_NOTHING)
    bol_preciounitario = models.CharField(max_length=100,verbose_name='Subtotal')

    class Meta:
        managed = False
        db_table = 'boleta'

    def __str__(self):
        return self.bol_cant


class BoletaInsumos(models.Model):
    bol_ins_id = models.AutoField(primary_key=True, auto_created=True, verbose_name= 'ID Boleta')
    bol_ins_emp = models.CharField(max_length=30, verbose_name='Empresa')
    bol_ins_fecha = models.DateTimeField(auto_now_add=True, verbose_name='Fecha')
    bol_ins_cant = models.IntegerField(verbose_name='Cantidad')
    bol_ins_desc = models.CharField(max_length=100, verbose_name='Descripcion')
    bol_ins_total = models.CharField(max_length=20, verbose_name='Total')
    prod_insumos_prod = models.ForeignKey('ProdInsumos', models.DO_NOTHING, verbose_name='ID Producto')

    class Meta:
        managed = False
        db_table = 'boleta_insumos'

    def __str__(self):
        return self.bol_ins_desc


class Cargo(models.Model):
    car_id = models.IntegerField(primary_key=True)
    car_nombre = models.CharField(max_length=30)
    car_abr = models.CharField(max_length=3)
    car_descripcion = models.CharField(max_length=50)
    boleta_bol = models.ForeignKey(Boleta, models.DO_NOTHING, blank=True, null=True)
    boleta_insumos_bol_ins = models.ForeignKey(BoletaInsumos, models.DO_NOTHING, blank=True, null=True)
    receta_rec = models.ForeignKey('Receta', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cargo'
    def __str__(self):
        return self.car_nombre


class CategoriaProducto(models.Model):
    cat_id = models.IntegerField(primary_key=True)
    cat_nombre = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'categoria_producto'
    def __str__(self):
        return self.cat_nombre

class Cliente(models.Model):
    cli_id = models.IntegerField(primary_key=True)
    cli_nombre = models.CharField(max_length=30)
    cli_usuario = models.CharField(max_length=30)
    cli_password = models.CharField(max_length=30)
    cli_correo = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'cliente'
    def __str__(self):
        return self.cli_nombre


class Pedido(models.Model):
    ped_id = models.FloatField(primary_key=True)
    ped_mesa = models.IntegerField()
    ped_pedido = models.CharField(max_length=50)
    ped_instucciones = models.CharField(max_length=200)
    ped_hra_pedido = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pedido'

    def __str__(self):
        return self.ped_pedido


class ProdInsumos(models.Model):
    prod_id = models.IntegerField(primary_key=True)
    prod_nombre = models.CharField(max_length=30)
    prod_descripcion = models.CharField(max_length=100)
    prod_fecha_ingreso = models.DateField()
    prod_categoria = models.CharField(max_length=30)
    proveedor_prov = models.ForeignKey('Proveedor', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'prod_insumos'
    def __str__(self):
        return self.prod_nombre


class Proveedor(models.Model):
    prov_id = models.IntegerField(primary_key=True)
    prov_nombre = models.CharField(max_length=30)
    prov_contacto = models.IntegerField()
    prov_direccion = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'proveedor'
    def __str__(self):
        return self.prov_nombre   


class Receta(models.Model):
    rec_id = models.IntegerField(primary_key=True)
    rec_nombre = models.CharField(max_length=30)
    rec_ingrediente = models.CharField(max_length=500)
    rec_preparacion = models.CharField(max_length=500)
    rec_comentario = models.CharField(max_length=30)
    categoria_producto_cat = models.ForeignKey(CategoriaProducto, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'receta'
    def __str__(self):
        return self.rec_nombre


class Reserva(models.Model):
    res_id = models.IntegerField(primary_key=True)
    res_mesa = models.IntegerField(blank=True, null=True)
    res_cantpersonas = models.IntegerField()
    res_fecha_solicitud = models.DateField()
    res_fecha_ingreso = models.DateField()
    cliente_cli = models.ForeignKey(Cliente, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'reserva'
    def __str__(self):
        return self.res_id  


class Trabajadores(models.Model):
    trab_id = models.IntegerField(primary_key=True)
    trab_rut = models.CharField(max_length=9)
    trab_fono = models.IntegerField()
    trab_correo = models.CharField(max_length=40)
    trab_password = models.CharField(max_length=30)
    trab_cargo = models.CharField(max_length=40)
    cargo_car = models.ForeignKey(Cargo, models.DO_NOTHING)
    trab_nombre = models.CharField(max_length=60)

    class Meta:
        managed = False
        db_table = 'trabajadores'
    def __str__(self):
        return self.trab_nombre